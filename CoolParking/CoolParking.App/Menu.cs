﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;

namespace CoolParking.App
{
    public class Menu : IDisposable
    {
        private readonly ParkingService _parkingService;

        public Menu()
        {
            var chargeMoneyTimer = new TimerService();
            var logTimer = new TimerService();
            var logService = new LogService(Settings.TransactionsLogFilePath);

            _parkingService = new ParkingService(chargeMoneyTimer, logTimer, logService);
        }

        public void Start(bool showMenu)
        {
            if (showMenu)
            {
                ShowMenu();
            }

            ConsoleKeyInfo key = Console.ReadKey();

            switch (key.Key)
            {
                case ConsoleKey.D1:
                    Console.WriteLine("\nEnter the balance of a vehicle: ");
                    var balanceString = Console.ReadLine();

                    bool intParseSesult = decimal.TryParse(balanceString, out var balance);
                    if (intParseSesult)
                    {
                        Console.WriteLine("\nEnter the type of a vehicle: ");
                        var vehicleTypeString = Console.ReadLine();

                        bool typeParseResult = Enum.TryParse(vehicleTypeString, out VehicleType type);
                        if (typeParseResult)
                        {
                            var id = Vehicle.GenerateRandomRegistrationPlateNumber();
                            var vehicle = new Vehicle(id, type, balance);
                            _parkingService.AddVehicle(vehicle);
                        }
                        else
                        {
                            Console.WriteLine("Wrong Type of a vehicle");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Wrong Balance");
                    }

                    ShowStandartMessage();
                    break;
                case ConsoleKey.D2:
                    Console.WriteLine("\nEnter the if of a vehicle you want to remove: ");
                    var vehicleIdString = Console.ReadLine();
                    try
                    {
                        _parkingService.RemoveVehicle(vehicleIdString);
                    }
                    catch (InvalidOperationException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    
                    ShowStandartMessage();
                    break;
                case ConsoleKey.D3:
                    var freeSpace = _parkingService.GetFreePlaces();
                    Console.WriteLine($"There are {freeSpace} free places on the parking");
                    ShowStandartMessage();
                    break;
                case ConsoleKey.D4:
                    var occupiedSpace = _parkingService.GetOccupiedSlotsNumber();
                    Console.WriteLine($"There are {occupiedSpace} occupied places on the parking");
                    ShowStandartMessage();
                    break;
                case ConsoleKey.D5:
                    var incomeForAllTime = _parkingService.GetBalance();
                    Console.WriteLine($"There are {incomeForAllTime} parking earned for all time");
                    ShowStandartMessage();
                    break;
                case ConsoleKey.D6:
                    var incomeForLastMinute = _parkingService.CountIncomeForLastMinute();
                    Console.WriteLine($"There are {incomeForLastMinute} parking earned for the last minute");
                    ShowStandartMessage();
                    break;
                case ConsoleKey.D7:
                    foreach (var vehicle in _parkingService.GetVehicles())
                    {
                        Console.WriteLine(vehicle);
                    }

                    ShowStandartMessage();
                    break;
                case ConsoleKey.D8:
                    var transactions = _parkingService.GetLastParkingTransactions();

                    Console.WriteLine("\n{0,-35}{1, 10}{2, 25}{3, 36}\n", "DateTime", "Money", "Type Of Charge",
                        "Id of the vehicle");

                    foreach (var t in transactions)
                    {
                        Console.WriteLine(
                            "{0,-35}{1, 10}{2, 25}{3, 40}",
                            t.TransactionDate.ToLongDateString() + t.TransactionDate.ToLongTimeString(),
                            t.Sum,
                            t.ChargeType,
                            t.VehicleId);
                    }

                    ShowStandartMessage();
                    break;
                case ConsoleKey.D9:
                    Console.WriteLine("\nEnter the id of a vehicle you want to top up the balance: ");
                    var vehicleIdToTopUpString = Console.ReadLine();
                    if (!string.IsNullOrWhiteSpace(vehicleIdToTopUpString))
                    {
                        Console.WriteLine("\nEnter the amount of money you want to top up: ");
                        var balanceToAddString = Console.ReadLine();

                        bool balanceParseResult = decimal.TryParse(balanceToAddString, out var balanceToAdd);
                        if (balanceParseResult)
                        {
                            _parkingService.TopUpVehicle(vehicleIdToTopUpString, balanceToAdd);
                        }
                        else
                        {
                            Console.WriteLine("Wrong Balance");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Sorry, id is in wrong format");
                    }

                    ShowStandartMessage();
                    break;
                case ConsoleKey.D0:
                    var logs = _parkingService.ReadFromLog();
                    Console.Write(logs);
                    ShowStandartMessage();
                    break;
                case ConsoleKey.R:
                    Start(true);
                    break;
                case ConsoleKey.C:
                    Console.Clear();
                    ShowStandartMessage();
                    break;
                case ConsoleKey.Escape:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Wrong key!");
                    ShowStandartMessage();
                    break;
            }
        }

        private void ShowMenu()
        {
            Console.WriteLine("\nTo add vehicle Press: 1"
                              + "\nTo remove vehicle Press: 2"
                              + "\nTo see number of free slots Press: 3"
                              + "\nTo see number of occupied slots Press: 4"
                              + "\nTo see amount of money that Parking earned for all time Press 5"
                              + "\nTo see amount of money that Parking earned for the last minute Press: 6"
                              + "\nTo see all vehicles on Parking Press: 7"
                              + "\nTo see all transactions for the last minute Press: 8"
                              + "\nTo top up the vehicle's balance Press: 9"
                              + "\nTo see the Transactions.log Parking Press: 0"
                              + "\nTo clear the console Press: C"
                              + "\nTo Exit from program Press: Esc");
        }

        private void ShowStandartMessage()
        {
            Console.WriteLine("\nPress Esc to exit or \"R\" to show menu or try previous commands again");
            Start(false);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _parkingService?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Menu()
        {
            Dispose(false);
        }
    }
}
