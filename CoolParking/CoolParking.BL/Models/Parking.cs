﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static readonly Lazy<Parking> _parking = new Lazy<Parking>(() => new Parking());

        private Parking()
        {
            Vehicles = new List<Vehicle>(Settings.ParkingSpace);
            Balance = Settings.ParkingInitialBalance;
        }

        public List<Vehicle> Vehicles { get; set; }

        public decimal Balance { get; set; }

        public List<TransactionInfo> Transactions { get; set; } = new List<TransactionInfo>();

        public static Parking GetParking()
        {
            return _parking.Value;
        }
    }
}