﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        private const string TransactionsLogFileName = "Transactions.log";

        public const decimal ParkingInitialBalance = 0M;
        public const int ParkingSpace = 10;
        public const int ChargeTimeout = 5;
        public const int LogTimeout = 60;
        public const decimal Fine = 2.5M;

        static Settings()
        {
            try
            {
                TransactionsLogFilePath = Path.Combine(Directory.GetCurrentDirectory(), TransactionsLogFileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static ReadOnlyDictionary<VehicleType, decimal> PriceList { get; } = new ReadOnlyDictionary<VehicleType, decimal>(
            new Dictionary<VehicleType, decimal>(4)
            {
                {VehicleType.PassengerCar, 2M},
                {VehicleType.Truck, 5M},
                {VehicleType.Bus, 3.5M},
                {VehicleType.Motorcycle, 1M},
            });

        public static string TransactionsLogFilePath { get; }

        public static string ExceptionsLogFilePath { get; }
    }
}