﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public static readonly Regex VehicleIdRegex = new Regex(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

        public Vehicle(string id, VehicleType typeOfVehicle, decimal balance)
        {
            if (!VehicleIdRegex.IsMatch(id))
                throw new ArgumentException("Vehicle id is invalid");
            if (balance < 0)
                throw new ArgumentException("Vehicle balance can't be negative");

            Id = id;
            Balance = balance;
            Type = typeOfVehicle;
        }

        public string Id { get; }
        public decimal Balance { get; internal set; }
        public VehicleType Type { get; }

        public override string ToString()
        {
            var message = Balance >= 0 ? $"has {Balance}$ money on the Balance" : $"has {Math.Abs(Balance)}$ money Debt";
            return $"Vehicle {Type} with id: {Id}, {message}";
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var numbers = "0123456789";
            var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var random = new Random();

            var firstLetters = $@"{letters[random.Next(letters.Length)]}{letters[random.Next(letters.Length)]}-";
            var idNumbers = $"{numbers[random.Next(numbers.Length)]}{numbers[random.Next(numbers.Length)]}{numbers[random.Next(numbers.Length)]}{numbers[random.Next(numbers.Length)]}";
            var secondLetters = $@"-{letters[random.Next(letters.Length)]}{letters[random.Next(letters.Length)]}";

            return firstLetters + idNumbers + secondLetters;
        }
    }
}
