﻿using System.Globalization;
using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public TransactionInfo(decimal sum, string vehicleId, ChargeType chargeType)
        {
            Sum = sum;
            VehicleId = vehicleId;
            ChargeType = chargeType;
        }

        public DateTime TransactionDate { get; } = DateTime.Now;
        public string VehicleId { get; }
        public decimal Sum { get; set; }
        public ChargeType ChargeType { get; set; }

        public override string ToString()
        {
            return
                $"Transaction was created in {TransactionDate.ToString(CultureInfo.InvariantCulture)}" +
                $"{Sum}$ money was charged from {ChargeType} of car with id: {VehicleId}";
        }
    }
}