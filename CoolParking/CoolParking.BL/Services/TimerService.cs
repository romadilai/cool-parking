﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer timer = new Timer();

        public double Interval
        {
            get => timer.Interval;
            set => timer.Interval = value;
        }

        public event ElapsedEventHandler Elapsed
        {
            add => timer.Elapsed += value;
            remove => timer.Elapsed -= value;
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

        public void Dispose()
        {
            timer.Dispose();
        }
    }
}