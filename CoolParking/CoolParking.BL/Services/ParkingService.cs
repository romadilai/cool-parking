﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private readonly ILogService _logger;

        private object _balanceLocker = new object();
        private object _vehicleLocker = new object();
        private object _transLocker = new object();
        private object _loggerLocker = new object();

        private readonly ITimerService _chargeMoneyTimer;
        private readonly ITimerService _writeLogsTimer;

        public ParkingService(ITimerService chargeMoneyTimer, ITimerService writeLogsTimer, ILogService logService)
        {
            _parking = Parking.GetParking();
            _logger = logService;

            _chargeMoneyTimer = chargeMoneyTimer;
            _writeLogsTimer = writeLogsTimer;
            
            _chargeMoneyTimer.Interval = Settings.ChargeTimeout * 1000;
            _chargeMoneyTimer.Elapsed += Charge;
            _chargeMoneyTimer.Start();

            _writeLogsTimer.Interval = Settings.LogTimeout * 1000;
            _writeLogsTimer.Elapsed += WriteLogs;
            _writeLogsTimer.Start();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }

        public bool IsFreeSpaceInTheParking => GetFreePlaces() > 0;

        public int GetCapacity()
        {
            return _parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Vehicles.Capacity - _parking.Vehicles.Count;
        }

        public int GetOccupiedSlotsNumber()
        {
            return _parking.Vehicles.Capacity - GetFreePlaces();
        }

        public string ReadFromLog()
        {
            lock (_loggerLocker)
            {
                return _logger.Read();
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            lock (_vehicleLocker)
            {
                if (_parking.Vehicles.Exists(v => v.Id == vehicle.Id))
                {
                    throw new ArgumentException("There are already such vehicle on the parking");
                }

                if (!IsFreeSpaceInTheParking)
                {
                    throw new ArgumentException("The parking is full. remove some vehicle and try again");
                }

                _parking.Vehicles.Add(vehicle);
                Console.WriteLine($"{vehicle} was successfully added on parking");
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            try
            {
                lock (_vehicleLocker)
                {
                    var vehicleToRemove = _parking.Vehicles.FirstOrDefault(c => c.Id == vehicleId);
                    if (vehicleToRemove == null)
                    {
                        throw new ArgumentException("Sorry, there no vehicles with such id");
                    }

                    if (vehicleToRemove.Balance < 0)
                    {
                        throw new InvalidOperationException("Sorry, vehicle has a debt before parking and can't be removed from parking. \nPopulate the balance of the vehicle and try again");
                    }

                    _parking.Vehicles.Remove(vehicleToRemove);
                    Console.WriteLine($"{vehicleToRemove} was successfully removed from parking");
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("Error occured while removing vehicle from parking. \nPlease try again later");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            // Validation
            if (sum <= 0)
            {
                throw new ArgumentException("Input positive amount of sum you want to top up");
            }

            try
            {
                lock (_vehicleLocker)
                {
                    var vehicleToTopUp = _parking.Vehicles.FirstOrDefault(c => c.Id == vehicleId);
                    if (vehicleToTopUp == null)
                    {
                        throw new ArgumentException("Sorry, there no vehicles with such id");
                    }

                    vehicleToTopUp.Balance += sum;
                    Console.WriteLine($"The balance of {vehicleId} was successfully topped up");
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("Error occured while topping up the vehicle. \nPlease try again later");
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            try
            {
                var timeMinuteAgo = DateTime.Now.AddMinutes(-1);
                lock (_transLocker)
                {
                    var transactions = _parking.Transactions.Where(t => t.TransactionDate > timeMinuteAgo).ToArray();
                    return transactions;
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);

                return new TransactionInfo[] { };
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex);

                return new TransactionInfo[] { };
            }
        }

        public decimal CountIncomeForLastMinute()
        {
            var transactions = GetLastParkingTransactions();

            return CountTransactionsIncome(transactions);
        }

        public decimal GetBalance() 
        {
            return _parking.Balance;
        }

        public override string ToString()
        {
            var priceList = "Price list:\n";
            foreach (var key in Settings.PriceList.Keys)
            {
                priceList += $"{key} - {Settings.PriceList[key]}\n";
            }

            return $"Single Parking has {GetFreePlaces()} free parking slots of {Settings.ParkingSpace}.\n" +
                   $"Current balance of parking is {_parking.Balance}$.\n {priceList}";
        }

        private void WriteLogs(object source, ElapsedEventArgs e)
        {
            var transactions = GetLastParkingTransactions();

            var dateNow = DateTime.Now;
            RemoveOldTransactions(dateNow.AddMinutes(-1));

            var sumOfTransactionsPayments = CountTransactionsIncome(transactions);

            lock (_loggerLocker)
            {
                var log = LogService.FormatLog($"{sumOfTransactionsPayments}$ Parking earned this minute", dateNow);
                _logger.Write(log);
            }
        }

        private void Charge(object source, ElapsedEventArgs e)
        {
            lock (_vehicleLocker)
            {
                foreach (var vehicle in _parking.Vehicles)
                {
                    if (!Settings.PriceList.TryGetValue(vehicle.Type, out var price)) continue;

                    var chargeType = ChargeType.Balance;

                    if (vehicle.Balance < price)
                    {
                        price *= Settings.Fine;
                        chargeType = ChargeType.Debt;
                    }

                    vehicle.Balance = vehicle.Balance - price;
                    lock (_balanceLocker)
                    {
                        _parking.Balance += price;
                    }

                    lock (_transLocker)
                    {
                        _parking.Transactions.Add(new TransactionInfo(price, vehicle.Id, chargeType));
                    }
                }
            }
        }

        private void RemoveOldTransactions(DateTime time)
        {
            try
            {
                lock (_transLocker)
                {
                    _parking.Transactions.RemoveAll(t => t.TransactionDate < time);
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e);
            }
        }

        private decimal CountTransactionsIncome(TransactionInfo[] transactions)
        {
            return transactions.Sum(t => t.Sum);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) return;

            _chargeMoneyTimer.Elapsed -= Charge;
            _writeLogsTimer.Elapsed -= WriteLogs;

            _chargeMoneyTimer?.Stop();
            _writeLogsTimer?.Stop();

            _chargeMoneyTimer?.Dispose();
            _writeLogsTimer?.Dispose();

            _parking.Vehicles.Clear();
            _parking.Transactions.Clear();
            _parking.Balance = 0M;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ParkingService()
        {
            Dispose(false);
        }
    }
}