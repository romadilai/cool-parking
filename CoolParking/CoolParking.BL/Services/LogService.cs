﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly object _locker = new object();

        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        private void WriteInfoToFile(string log, bool append)
        {
            using (var tw = new StreamWriter(LogPath, append))
            {
                tw.WriteLine(log);
            }
        }

        public void Write(string logInfo)
        {
            lock (_locker)
            {
                try
                {
                    if (File.Exists(LogPath))
                    {
                        WriteInfoToFile(logInfo, true);
                    }
                    else
                    {
                        File.Create(LogPath).Dispose();
                        WriteInfoToFile(logInfo, false);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public string Read()
        {
            lock (_locker)
            {
                if (!File.Exists(Settings.TransactionsLogFilePath))
                {
                    throw new InvalidOperationException("Log File with that path doesn't exist");
                }

                try
                {
                    using (StreamReader r = File.OpenText(Settings.TransactionsLogFilePath))
                    {
                        return r.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    return "Error occurred while trying read from Transactions.log";
                }
            }
        }

        public static string FormatLog(string info, DateTime? date = null)
        {
            var logDate = date ?? DateTime.Now;
            return $"{logDate.ToLongDateString()} {logDate.ToLongTimeString()} : {info}";
        }
    }
}